from airflow.decorators import task

from stock_screener_pipeline.source.utils import(
    delete_keys_from_s3,
    download_dict_from_s3,
    upload_dict_to_s3,
)

@task
def transform_data_task(collected_s3_key):
    collected_data = download_dict_from_s3(collected_s3_key)
    variation_dict = {}
    for term_period, symbols_data in collected_data.items():
        term_data = {}
        for symbol, history_data in symbols_data.items():
            variation = ((history_data["close"] - history_data["open"]) / history_data['open']) * 100
            term_data.update({symbol: variation})
        variation_dict[term_period] = term_data
    delete_keys_from_s3([collected_s3_key])
    return upload_dict_to_s3("transformed-data", variation_dict)
