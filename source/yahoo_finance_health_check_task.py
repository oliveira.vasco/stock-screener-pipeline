from airflow.decorators import task


@task
def is_yahoo_finance_alive() -> None:
    from yfinance import Ticker
    from pandas.core.frame import DataFrame
    ticker = Ticker("SPY")
    history_data_frame = ticker.history()
    assert isinstance(history_data_frame, DataFrame)
    assert not history_data_frame.empty
