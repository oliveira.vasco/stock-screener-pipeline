# Stock Screener Pipeline
## Task description
1. Checks if Yahoo finance is alive by querying;
2. Extracts stock information from Yahoo finance;
3. Calculates rentability for pre-defined periods;
4. Sends rentability information to e-mail.

![](pipeline.png)
