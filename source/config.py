from typing import List, Dict

TERMS: List[Dict[str, str]] = [
    {"period": "1d", "interval": "1h"},
    {"period": "3d", "interval": "1h"},
    {"period": "1wk", "interval": "1d"},
    {"period": "1mo", "interval": "1mo"},
    {"period": "6mo", "interval": "1mo"},
    {"period": "1y", "interval": "3mo"},
]

SYMBOLS = ["SPYJ.DE"]

# Configs to be adjusted
AWS_CONN_ID = "DefaultConnId"
AWS_BUCKET_ID = "bucket-id"
AWS_S3_KEY_PREFIX = "key-prefix"

EMAIL_SUBJECT = "Stock screener collection"
MAIL_GUN_BASE_URL = "mail-gun-url"
MAIL_GUN_API_SECRET = "mail-gun-token"
FROM_EMAIL = "sender-email"
TO_EMAIL = "receiver-email"
