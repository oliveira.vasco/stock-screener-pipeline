import json
import requests
from datetime import date, datetime
from typing import List

from airflow.providers.amazon.aws.hooks.s3 import S3Hook

from stock_screener_pipeline.source.config import (
    AWS_BUCKET_ID,
    AWS_CONN_ID,
    AWS_S3_KEY_PREFIX,
    EMAIL_SUBJECT,
    MAIL_GUN_BASE_URL,
    MAIL_GUN_API_SECRET,
    FROM_EMAIL,
    TO_EMAIL,
)



def upload_dict_to_s3(content_type: str, content: dict) -> str:
    s3_key = f"{AWS_S3_KEY_PREFIX}/{content_type}-{datetime.timestamp(datetime.now())}.json"
    s3_hook = S3Hook(aws_conn_id=AWS_CONN_ID)
    s3_hook.load_string(string_data=json.dumps(content), key=s3_key, bucket_name=AWS_BUCKET_ID)
    return s3_key


def download_dict_from_s3(key: str) -> dict:
    s3_hook = S3Hook(aws_conn_id=AWS_CONN_ID)
    return json.loads(s3_hook.read_key(key=key, bucket_name=AWS_BUCKET_ID))


def delete_keys_from_s3(keys: List[str]) -> str:
    s3_hook = S3Hook(aws_conn_id=AWS_CONN_ID)
    return s3_hook.delete_objects(bucket=AWS_BUCKET_ID, keys=keys)


def send_email(content: str):
    subject = f"{EMAIL_SUBJECT} {date.today()}"
    return requests.post(
        MAIL_GUN_BASE_URL,
        auth = ("api", MAIL_GUN_API_SECRET),
        data = {
            "from": FROM_EMAIL,
            "to": TO_EMAIL,
            "subject": subject,
            "text": content
            }
        )
