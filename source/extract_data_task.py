import json
from datetime import datetime

from airflow.decorators import task
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from yfinance import Ticker

from stock_screener_pipeline.source.config import SYMBOLS, TERMS
from stock_screener_pipeline.source.utils import upload_dict_to_s3


def _extract_data():
    extracted_data = {}
    for term_info in TERMS:
        term_data = {}
        for symbol in SYMBOLS:
            ticker = Ticker(symbol)
            history_data_frame = ticker.history(period=term_info["period"], interval=term_info["interval"])
            if not history_data_frame.empty:
                term_data.update({symbol: {"close": history_data_frame["Close"][-1], "open": history_data_frame["Open"][0]}})
        extracted_data[term_info["period"]] = term_data
    return extracted_data


@task
def extract_data_task():
    extracted_data = _extract_data()
    return upload_dict_to_s3("extracted-data", extracted_data)
