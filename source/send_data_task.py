from airflow.decorators import task

from pandas import DataFrame

from stock_screener_pipeline.source.utils import (
    delete_keys_from_s3,
    download_dict_from_s3,
    send_email,
)


@task
def send_data_task(transformed_s3_key: str, sort_by="1wk"):
    transformed_data = download_dict_from_s3(transformed_s3_key)
    transformed_frame = DataFrame(transformed_data)
    transformed_frame.sort_values(by=sort_by, inplace=True)
    transformed_frame.round(2)
    send_email(str(transformed_frame))
    delete_keys_from_s3([transformed_s3_key])
