from airflow.decorators import dag

from stock_screener_pipeline.source.yahoo_finance_health_check_task import is_yahoo_finance_alive
from stock_screener_pipeline.source.extract_data_task import extract_data_task
from stock_screener_pipeline.source.transform_data_task import transform_data_task
from stock_screener_pipeline.source.send_data_task import send_data_task

from datetime import datetime

default_args = {
    "start_date": datetime(2022, 1, 1)
}


@dag(schedule_interval="10 7,18 * * *", default_args=default_args, catchup=False)
def stock_screener_dag():
    live = is_yahoo_finance_alive()
    collected_key = extract_data_task()
    transformed_key = transform_data_task(collected_key)
    sent = send_data_task(transformed_key)
    live >> collected_key >> transformed_key >> sent

dag = stock_screener_dag()
